import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from '@app/modules/material/material.module';
import { TranslateModule } from '@ngx-translate/core';
import { DownComponent } from './down.component';

describe('DownComponent', () => {
  let component: DownComponent;
  let fixture: ComponentFixture<DownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DownComponent],
      imports: [MaterialModule, TranslateModule.forRoot()]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
