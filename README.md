## A simple counter developped with Angular & NgRx

+ When the increment button is clicked, the counter increases by 1
+ When the decrement button is clicked, the counter decreases by 1
+ When the counter reaches 10, the background color changes to #e74c3c
+ When the counter reaches -10, the background color changes to #27ae60
+ The increment / decrement coef is multiplied by 2 every 30 actions